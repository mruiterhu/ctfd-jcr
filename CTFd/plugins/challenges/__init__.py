import flask
import CTFd
import time
import datetime
import sys

CHALLENGE_CLASSES = {}

def get_chal_class(class_id):
    cls = CHALLENGE_CLASSES.get(class_id)
    if cls is None:
        raise KeyError
    return cls

class BaseChallenge(object):
    id = None
    name = None
    templates = {}
    scripts = {}
    challenge_model = CTFd.models.Challenges

    @classmethod
    def create(cls, request):
        data = request.form or request.get_json()
        challenge = cls.challenge_model(**data)
        CTFd.models.db.session.add(challenge)
        CTFd.models.db.session.commit()
        return challenge

    @classmethod
    def read(cls, challenge):
        data = {
            "id": challenge.id,
            "name": challenge.name,
            "value": challenge.value,
            "description": challenge.description,
            "connection_info": challenge.connection_info,
            "category": challenge.category,
            "state": challenge.state,
            "max_attempts": challenge.max_attempts,
            "type": challenge.type,
            "type_data": {
                "id": cls.id,
                "name": cls.name,
                "templates": cls.templates,
                "scripts": cls.scripts,
            },
        }
        return data

    @classmethod
    def update(cls, challenge, request):
        data = request.form or request.get_json()
        for attr, value in data.items():
            setattr(challenge, attr, value)
        CTFd.models.db.session.commit()
        return challenge

    @classmethod
    def delete(cls, challenge):
        CTFd.models.Fails.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Solves.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Flags.query.filter_by(challenge_id=challenge.id).delete()
        files = CTFd.models.ChallengeFiles.query.filter_by(challenge_id=challenge.id).all()
        for f in files:
            CTFd.utils.uploads.delete_file(f.id)
        CTFd.models.ChallengeFiles.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Tags.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Hints.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Challenges.query.filter_by(id=challenge.id).delete()
        cls.challenge_model.query.filter_by(id=challenge.id).delete()
        CTFd.models.db.session.commit()

    @classmethod
    def attempt(cls, challenge, request):
        data = request.form or request.get_json()
        submission = data["submission"].strip()
        flags = CTFd.models.Flags.query.filter_by(challenge_id=challenge.id).all()
        for flag in flags:
            try:
                if CTFd.plugins.flags.get_flag_class(flag.type).compare(flag, submission):
                    return True, "Correct"
            except CTFd.plugins.flags.FlagException as e:
                return False, str(e)
        return False, "Incorrect"

    @classmethod
    def solve(cls, user, team, challenge, request):
        data = request.form or request.get_json()
        submission = data["submission"].strip()
        solve = CTFd.models.Solves(
            user_id=user.id,
            team_id=team.id if team else None,
            challenge_id=challenge.id,
            ip=CTFd.utils.user.get_ip(req=request),
            provided=submission,
        )
        CTFd.models.db.session.add(solve)
        CTFd.models.db.session.commit()

    @classmethod
    def fail(cls, user, team, challenge, request):
        data = request.form or request.get_json()
        submission = data["submission"].strip()
        wrong = CTFd.models.Fails(
            user_id=user.id,
            team_id=team.id if team else None,
            challenge_id=challenge.id,
            ip=CTFd.utils.user.get_ip(request),
            provided=submission,
        )
        CTFd.models.db.session.add(wrong)
        CTFd.models.db.session.commit()

class StandardChallengeType(BaseChallenge):
    id = "standard"
    name = "standard"
    templates = {
        "create": "/plugins/challenges/assets/create.html",
        "update": "/plugins/challenges/assets/update.html",
        "view": "/plugins/challenges/assets/view.html",
    }
    scripts = {
        "create": "/plugins/challenges/assets/create.js",
        "update": "/plugins/challenges/assets/update.js",
        "view": "/plugins/challenges/assets/view.js",
    }
    route = "/plugins/challenges/assets/"
    blueprint = flask.Blueprint("standard", __name__, template_folder="templates", static_folder="assets")
    challenge_model = CTFd.models.Challenges

class StandardChallengeLog(CTFd.models.db.Model):
    __tablename__ = "standard_log"
    __table_args__ = (CTFd.models.db.UniqueConstraint("id"), {})

    id = CTFd.models.db.Column(CTFd.models.db.Integer, primary_key=True)
    challenge_id = CTFd.models.db.Column(CTFd.models.db.Integer, index=True)
    user_id = CTFd.models.db.Column(CTFd.models.db.Integer, index=True)
    start_epoch = CTFd.models.db.Column(CTFd.models.db.Integer, index=True)

    def __init__(self, **kwargs):
        super(StandardChallengeLog, self).__init__(**kwargs)

def load(app):
    # Create database tables
    app.db.create_all()
    # Register challenge type
    CHALLENGE_CLASSES["standard"] = StandardChallengeType
    # Register plugin assets
    CTFd.plugins.register_plugin_assets_directory(app, base_path="/plugins/challenges/assets/")
    # Initialize Flask blueprint
    standard_challenges = flask.Blueprint("standard_challenges", __name__, template_folder="templates", static_folder="assets")
    # Initialize start route
    @standard_challenges.route("/standard_challenges/start", methods=["POST"])
    def start_challenge():
        # Temporary values
        data = flask.request.get_json()
        challenge_id = data["challenge_id"]
        user_id = data["user_id"]
        current_epoch = int(time.time())
        # Save start time
        statistics = StandardChallengeLog(challenge_id=int(challenge_id), user_id=int(user_id), start_epoch=current_epoch)
        flask.current_app.db.session.query(StandardChallengeLog).filter_by(challenge_id=int(challenge_id), user_id=int(user_id)).delete()
        flask.current_app.db.session.add(statistics)
        flask.current_app.db.session.commit()
        # Log start of challenge
        log_prefix = "[TRACK] [" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
        log_suffix = "\n"
        sys.stdout.write(log_prefix +
                        "challenge:\"" + challenge_id + "\" --- " +
                        "user:\"" + user_id + "\" --- " +
                        "seconds:\"0\"" +
                        log_suffix)
        # Return final result
        return "{}", 200
    # Initialize stop route
    @standard_challenges.route("/standard_challenges/stop", methods=["POST"])
    def stop_challenge():
        # Temporary values
        data = flask.request.get_json()
        challenge_id = data["challenge_id"]
        user_id = data["user_id"]
        current_epoch = int(time.time())
        # Retrieve log
        statistics = flask.current_app.db.session.query(StandardChallengeLog).filter_by(challenge_id=int(challenge_id), user_id=int(user_id)).first()
        # Check if logging
        if statistics:
            # Calculate seconds
            seconds = (current_epoch - statistics.start_epoch)
            # Log stop of challenge
            log_prefix = "[TRACK] [" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
            log_suffix = "\n"
            sys.stdout.write(log_prefix +
                    "challenge:\"" + challenge_id + "\" --- " +
                    "user:\"" + user_id + "\" --- " +
                    "seconds:\"" + str(seconds) + "\"" +
                    log_suffix)
            # Delete log
            flask.current_app.db.session.query(StandardChallengeLog).filter_by(challenge_id=challenge_id, user_id=user_id).delete()
        # Return final result
        return "{}", 200
    # Register Flask blueprint
    app.register_blueprint(standard_challenges)