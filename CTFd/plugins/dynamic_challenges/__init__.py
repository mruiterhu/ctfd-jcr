import math
from flask import current_app, request
import time
import datetime
import sys

from flask import Blueprint

from CTFd.models import Challenges, Solves, db
from CTFd.plugins import register_plugin_assets_directory
from CTFd.plugins.challenges import CHALLENGE_CLASSES, BaseChallenge
from CTFd.plugins.migrations import upgrade
from CTFd.utils.modes import get_model


class DynamicChallenge(Challenges):
    __mapper_args__ = {"polymorphic_identity": "dynamic"}
    id = db.Column(
        db.Integer, db.ForeignKey("challenges.id", ondelete="CASCADE"), primary_key=True
    )
    initial = db.Column(db.Integer, default=0)
    minimum = db.Column(db.Integer, default=0)
    decay = db.Column(db.Integer, default=0)

    def __init__(self, *args, **kwargs):
        super(DynamicChallenge, self).__init__(**kwargs)
        self.value = kwargs["initial"]


class DynamicChallengeType(BaseChallenge):
    id = "dynamic"  # Unique identifier used to register challenges
    name = "dynamic"  # Name of a challenge type
    templates = {  # Handlebars templates used for each aspect of challenge editing & viewing
        "create": "/plugins/dynamic_challenges/assets/create.html",
        "update": "/plugins/dynamic_challenges/assets/update.html",
        "view": "/plugins/dynamic_challenges/assets/view.html",
    }
    scripts = {  # Scripts that are loaded when a template is loaded
        "create": "/plugins/dynamic_challenges/assets/create.js",
        "update": "/plugins/dynamic_challenges/assets/update.js",
        "view": "/plugins/dynamic_challenges/assets/view.js",
    }
    # Route at which files are accessible. This must be registered using register_plugin_assets_directory()
    route = "/plugins/dynamic_challenges/assets/"
    blueprint = Blueprint("dynamic_challenges", __name__,
                          template_folder="templates", static_folder="assets")
    challenge_model = DynamicChallenge

    @classmethod
    def calculate_value(cls, challenge):
        Model = get_model()

        solve_count = (
            Solves.query.join(Model, Solves.account_id == Model.id)
            .filter(
                Solves.challenge_id == challenge.id,
                Model.hidden == False,
                Model.banned == False,
            )
            .count()
        )

        # If the solve count is 0 we shouldn't manipulate the solve count to
        # let the math update back to normal
        if solve_count != 0:
            # We subtract -1 to allow the first solver to get max point value
            solve_count -= 1

        # It is important that this calculation takes into account floats.
        # Hence this file uses from __future__ import division
        value = (
            ((challenge.minimum - challenge.initial) / (challenge.decay ** 2))
            * (solve_count ** 2)
        ) + challenge.initial

        value = math.ceil(value)

        if value < challenge.minimum:
            value = challenge.minimum

        challenge.value = value
        db.session.commit()
        return challenge

    @classmethod
    def read(cls, challenge):
        challenge = DynamicChallenge.query.filter_by(id=challenge.id).first()
        data = {
            "id": challenge.id,
            "name": challenge.name,
            "value": challenge.value,
            "initial": challenge.initial,
            "decay": challenge.decay,
            "minimum": challenge.minimum,
            "description": challenge.description,
            "connection_info": challenge.connection_info,
            "category": challenge.category,
            "state": challenge.state,
            "max_attempts": challenge.max_attempts,
            "type": challenge.type,
            "type_data": {
                "id": cls.id,
                "name": cls.name,
                "templates": cls.templates,
                "scripts": cls.scripts,
            },
        }
        return data

    @classmethod
    def update(cls, challenge, request):
        """
        This method is used to update the information associated with a challenge. This should be kept strictly to the
        Challenges table and any child tables.

        :param challenge:
        :param request:
        :return:
        """
        data = request.form or request.get_json()

        for attr, value in data.items():
            # We need to set these to floats so that the next operations don't operate on strings
            if attr in ("initial", "minimum", "decay"):
                value = float(value)
            setattr(challenge, attr, value)

        return DynamicChallengeType.calculate_value(challenge)

    @classmethod
    def solve(cls, user, team, challenge, request):
        super().solve(user, team, challenge, request)

        DynamicChallengeType.calculate_value(challenge)


class DynamicChallengeLog(db.Model):
    __tablename__ = "dynamic_log"
    __table_args__ = (db.UniqueConstraint("id"), {})

    id = db.Column(db.Integer, primary_key=True)
    challenge_id = db.Column(db.Integer, index=True)
    user_id = db.Column(db.Integer, index=True)
    start_epoch = db.Column(db.Integer, index=True)

    def __init__(self, **kwargs):
        super(DynamicChallengeLog, self).__init__(**kwargs)


def load(app):
    # Create database tables
    app.db.create_all()
    # Register challenge type
    CHALLENGE_CLASSES["dynamic"] = DynamicChallengeType
    # Register plugin assets
    register_plugin_assets_directory(
        app, base_path="/plugins/dynamic_challenges/assets/")
    # Initialize Flask blueprint
    dynamic_challenges = Blueprint(
        "dynamic_challenges", __name__, template_folder="templates", static_folder="assets")
    # Initialize start route

    @dynamic_challenges.route("/dynamic_challenges/start", methods=["POST"])
    def start_challenge():
        # Temporary values
        data = request.get_json()
        challenge_id = data["challenge_id"]
        user_id = data["user_id"]
        current_epoch = int(time.time())
        # Save start time
        statistics = DynamicChallengeLog(challenge_id=int(
            challenge_id), user_id=int(user_id), start_epoch=current_epoch)
        current_app.db.session.query(DynamicChallengeLog).filter_by(
            challenge_id=int(challenge_id), user_id=int(user_id)).delete()
        current_app.db.session.add(statistics)
        current_app.db.session.commit()
        # Log start of challenge
        log_prefix = "[TRACK] [" + \
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
        log_suffix = "\n"
        sys.stdout.write(log_prefix +
                         "challenge:\"" + challenge_id + "\" --- " +
                         "user:\"" + user_id + "\" --- " +
                         "seconds:\"0\"" +
                         log_suffix)
        # Return final result
        return "{}", 200

    # Initialize stop route
    @dynamic_challenges.route("/dynamic_challenges/stop", methods=["POST"])
    def stop_challenge():
        # Temporary values
        data = request.get_json()
        challenge_id = data["challenge_id"]
        user_id = data["user_id"]
        current_epoch = int(time.time())
        # Retrieve log
        statistics = current_app.db.session.query(DynamicChallengeLog).filter_by(
            challenge_id=int(challenge_id), user_id=int(user_id)).first()
        # Check if logging
        if statistics:
            # Calculate seconds
            seconds = (current_epoch - statistics.start_epoch)
            # Log stop of challenge
            log_prefix = "[TRACK] [" + \
                datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
            log_suffix = "\n"
            sys.stdout.write(log_prefix +
                             "challenge:\"" + challenge_id + "\" --- " +
                             "user:\"" + user_id + "\" --- " +
                             "seconds:\"" + str(seconds) + "\"" +
                             log_suffix)
            # Delete log
            current_app.db.session.query(DynamicChallengeLog).filter_by(
                challenge_id=challenge_id, user_id=user_id).delete()
        # Return final result
        return "{}", 200
    # Register Flask blueprint
    app.register_blueprint(dynamic_challenges)
