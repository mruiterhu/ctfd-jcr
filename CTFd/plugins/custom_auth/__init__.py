import flask
import werkzeug.routing

from .code import surfconext
from .code import register
from .code import confirm
from .code import login
from .code import reset
from .code import settings

def load(app):
    # Initialize Flask blueprint
    custom_auth = flask.Blueprint("custom_auth", __name__, template_folder="templates", static_folder="assets")
    # Initialize SURFconext
    surfconext.load(custom_auth)
    # Modify Flask routes
    app.url_map.add(werkzeug.routing.Rule('/settings', endpoint='views.settings', methods=['GET', 'POST']))
    # Overwrite Flask routes
    app.view_functions["auth.oauth"] = lambda: ("", 404)
    app.view_functions["auth.redirect"] = lambda: ("", 404)
    app.view_functions["auth.register"] = register.handle
    app.view_functions["auth.confirm"] = confirm.handle
    app.view_functions["auth.login"] = login.handle
    app.view_functions["auth.reset_password"] = reset.handle
    app.view_functions["views.settings"] = settings.handle
    # Register Flask blueprint
    app.register_blueprint(custom_auth)