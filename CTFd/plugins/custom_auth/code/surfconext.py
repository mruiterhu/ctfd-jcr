import flask
import CTFd
import sys
import datetime

from authlib.integrations.flask_client import oauth_registry

def load(custom_auth):
    # Temporary values
    mustExit = False
    error_prefix = "[ERROR] [" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
    error_suffix = "\n"
    # Retrieve configurations
    SETUP_ACCOUNT_TYPE = CTFd.utils.get_app_config("SETUP_ACCOUNT_TYPE")
    SETUP_EVENT_ADMIN = CTFd.utils.get_app_config("SETUP_EVENT_ADMIN")
    SURF_META = CTFd.utils.get_app_config("SURF_META")
    SURF_SCHEME = CTFd.utils.get_app_config("SURF_SCHEME")
    SURF_CLIENT_ID = CTFd.utils.get_app_config("SURF_CLIENT_ID")
    SURF_CLIENT_SECRET = CTFd.utils.get_app_config("SURF_CLIENT_SECRET")
    # Check if SURF system is enabled
    if SETUP_ACCOUNT_TYPE == "surf" or SETUP_ACCOUNT_TYPE == "both":
        # Validate empty surf configurations
        if not SURF_META:
            sys.stderr.write(error_prefix + "value of ENV variable \"SURF_META\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SURF_SCHEME:
            sys.stderr.write(error_prefix + "value of ENV variable \"SURF_SCHEME\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SURF_CLIENT_ID:
            sys.stderr.write(error_prefix + "value of ENV variable \"SURF_CLIENT_ID\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SURF_CLIENT_SECRET:
            sys.stderr.write(error_prefix + "value of ENV variable \"SURF_CLIENT_SECRET\" cannot be empty!" + error_suffix)
            mustExit = True
        if mustExit:
            sys.exit(1)
        # Validate invalid surf configurations
        if SURF_SCHEME != "http" and SURF_SCHEME != "https":
            sys.stderr.write(error_prefix + "value of ENV variable \"SURF_SCHEME\" cannot be invalid!" + error_suffix)
            mustExit = True
        if mustExit:
            sys.exit(1)
    # Initialize OAuth
    oauth = oauth_registry.OAuth(flask.current_app)
    # Register SURFconext
    oauth.register(
        "surfconext",
        server_metadata_url=SURF_META,
        client_id=SURF_CLIENT_ID,
        client_secret=SURF_CLIENT_SECRET,
        client_kwargs={"scope": "openid"}
    )
    # Initialize authorize route
    @custom_auth.route("/auth/surfconext/authorize", methods=["GET"])
    def surfconext_authorize():
        # Connect to SURFconext
        return oauth.surfconext.authorize_redirect(flask.url_for("custom_auth.surfconext_confirm", _external=True, _scheme=SURF_SCHEME))
    # Initialize confirm route
    @custom_auth.route("/auth/surfconext/confirm", methods=["GET"])
    def surfconext_confirm():
        # Temporary values
        infos = CTFd.utils.helpers.get_infos()
        errors = CTFd.utils.helpers.get_errors()
        # Retrieve SURFconext account information
        token = oauth.surfconext.authorize_access_token()
        oauth.surfconext.parse_id_token(token)
        surf_account = oauth.surfconext.userinfo()
        # Try to find existing CTFd account
        account = CTFd.models.Users.query.filter_by(email=surf_account["email"]).first()
        mustCreateAccount = not account
        # Create CTFd account
        if mustCreateAccount:
            # Extract name
            name = surf_account["name"].strip()
            # Extract email
            email_address = surf_account["email"].strip().lower()
            # Extract type
            type = "user"
            if email_address == SETUP_EVENT_ADMIN:
                type = "admin"
            # Extract affiliation
            if "student" in surf_account["eduperson_affiliation"]:
                affiliation = "Student"
            elif "employee" in surf_account["eduperson_affiliation"]:
                affiliation = "Employee"
            # Extract website
            website = "https://" + surf_account["schac_home_organization"]
            # Create new CTFd account
            account = CTFd.models.Users(
                name=name,
                email=email_address,
                type=type,
                affiliation=affiliation,
                website=website,
                country="NL",
                verified=True)
        # Check if account is not from organization
        if not mustCreateAccount and account.affiliation == "Unknown":
            # Determine login page
            if (SETUP_ACCOUNT_TYPE == "ctfd"):
                LOGIN_PAGE = "login_page_ctfd.html"
            if (SETUP_ACCOUNT_TYPE == "surf"):
                LOGIN_PAGE = "login_page_surf.html"
            if (SETUP_ACCOUNT_TYPE == "both"):
                LOGIN_PAGE = "login_page_both.html"
            # Notify user
            errors.append("Organizational email already in use!")
            CTFd.models.db.session.close()
            return flask.render_template(LOGIN_PAGE, infos=infos, errors=errors)
        # Reload Flask session to prevent session fixation
        flask.session.regenerate()
        # Open new database session
        with flask.current_app.app_context():
            # Add CTFd account to database
            if mustCreateAccount:
                CTFd.models.db.session.add(account)
                CTFd.models.db.session.commit()
            # Login CTFd account
            CTFd.utils.security.auth.login_user(account)
            # Go to index page
            CTFd.models.db.session.close()
            return flask.redirect("/")
