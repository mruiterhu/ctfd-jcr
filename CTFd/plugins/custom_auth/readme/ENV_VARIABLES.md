# ENV Variables

There are 4 ENV variables that you **must** specify:

- SURF_META
- SURF_SCHEME
- SURF_CLIENT_ID
- SURF_CLIENT_SECRET

## 1. SURF_META

The plugin needs to have some metadata in order to be able to connect to the servers of SURFconext. The value of this ENV variable can either be 1 of the following URLs:

- <https://connect.surfconext.nl/.well-known/openid-configuration> (PRODUCTION ENVIRONMENT)
- <https://connect.test.surfconext.nl/.well-known/openid-configuration> (TEST ENVIRONMENT)

## 2. SURF_SCHEME

The plugin needs to know with which scheme (either `http` or `https`) it must connect to SURFconext.

## 3. SURF_CLIENT_ID

The plugin needs to know which SURFconext entity it needs to connect to. You can find the ID under the `Entity ID` column in the [SURFconext dashboard](https://sp.surfconext.nl/).

## 4. SURF_CLIENT_SECRET

The plugin needs to be able to access the SURFconext entity using a secret. This secret is shown to you **once** when you generate the client secret. This can be done with the `Reset client secret` button in the [SURFconext dashboard](https://sp.surfconext.nl/).