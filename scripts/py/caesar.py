#Code source: https://housewifehacker.blogspot.com/2010/10/caesar-cipher-in-python-using-ascii.html

#flag will generate a flag in a specific syntax
import flag
#string will later allow us to test for ascii letters
import string

def encode(input,shift):
  #create an empty string
  foo = ''
  #a for loop to shift the letters by the shift input 
  for x in input:
    #changes only ascii letters
    if x in string.ascii_letters:
      #upper case end of alphabet, to loop back to A
      if ord(x) > ord("Z") - shift and ord(x) <= ord("Z"):
        new_ord = ord(x) + shift - 26
      #lower case end of alphabet
      elif ord(x) > ord("z") - shift and ord(x) <= ord("z"):
        new_ord = ord(x) + shift - 26
      else:
        new_ord = ord(x) + shift
    #other characters will remain unchanged
    else:
      new_ord = ord(x)

    new_chr = chr(new_ord)
    foo += new_chr

  print("Your encoded message is ", foo)

encode(input = flag.create_flag(), shift = 1)