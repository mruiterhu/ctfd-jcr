import openpyxl
import csv
import inquirer

def interactive():
    questions = [
    inquirer.List('conversion',
                    message="What conversion do you want to make?",
                    choices=['From CSV to Excel', 'From Excel to CSV'],
                ),
    ]
    answers = inquirer.prompt(questions)

    if answers["conversion"] == "From CSV to Excel":
        filepath = [
        inquirer.Path('CSV',
                        message="Where is your CSV input file located?",
                        exists=True,
                        path_type=inquirer.Path.FILE
                        ),
        ]
        filepath = inquirer.prompt(filepath).get('CSV')
        csv2xlsx(filepath)
    elif answers["conversion"] == "From Excel to CSV":
        filepath = [
        inquirer.Path('XLSX',
                        message="Where is your XLSX input file located?",
                        exists=True,
                        path_type=inquirer.Path.FILE
                        ),
        ]
        filepath = inquirer.prompt(filepath).get('XLSX')
        xlsx2csv(filepath)
    else:
        print("Oops! Something went wrong !@#?")


def csv2xlsx(filepath):
    wb = openpyxl.Workbook()
    ws = wb.active
    with open(filepath) as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            ws.append(row)
    wb.save('file.xlsx')


def xlsx2csv(filepath):
    wb = openpyxl.load_workbook(filepath)
    sh = wb.active # was .get_active_sheet()
    with open('file.csv', 'w', newline="") as f:
        c = csv.writer(f)
        for r in sh.iter_rows(): # generator; was sh.rows
            c.writerow([cell.value for cell in r])

interactive()
