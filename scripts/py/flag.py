import secrets

def create_flag():
    syntax = "CTFlagXXj01n7XXcYB3RXXR4n93"
    token = str(secrets.token_hex(32))
    flag = str(syntax + "XX" + token +"XX")

    return(flag)

#Manual
#print(create_flag())